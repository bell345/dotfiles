#!/usr/bin/env sh

killall -q polybar

while pgrep -x polybar >/dev/null; do sleep 1; done

MONITOR=HDMI-0 polybar -r left &
MONITOR=VGA-0  polybar -r right &
MONITOR=HDMI-0 polybar -r bottom-left &
MONITOR=VGA-0  polybar -r bottom-right &

"$HOME/sh/rewrite.py" "$HOME/.config/polybar/config"

