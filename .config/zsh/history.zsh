#!/bin/zsh

HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000
# append instead of clobbering
setopt append_history
# only append entries upon exit
unsetopt inc_append_history

