#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:ts=2:sw=2:expandtab
#
# adapted from:
# https://gist.github.com/Airblader/3a96a407e16dae155744
#

import os
import sys
import xcffib as xcb
from xcffib.xproto import *
from PIL import Image, ImageEnhance

XCB_MAP_STATE_VIEWABLE = 2

BRIGHTNESS_FACTOR = 0.4
CONTRAST_FACTOR = 1.5

TARGET_PATH = "/tmp/locked.png"
if len(sys.argv) > 1:
    TARGET_PATH = sys.argv[1]

def screenshot():
  os.system('import -window root {}'.format(TARGET_PATH))

def xcb_fetch_windows():
  """ Returns an array of rects of currently visible windows. """

  x = xcb.connect()
  root = x.get_setup().roots[0].root

  rects = []

  # iterate through top-level windows
  for child in x.core.QueryTree(root).reply().children:
    # make sure we only consider windows that are actually visible
    attributes = x.core.GetWindowAttributes(child).reply()
    if attributes.map_state != XCB_MAP_STATE_VIEWABLE:
      continue

    # exclude windows labelled as "root" windows
    wm_classes = b''.join(
        x.core.GetProperty(
          False, child, Atom.WM_CLASS, GetPropertyType.Any, 0, 256
        ).reply().value
      ).split(b'\x00')

    if b'root' in wm_classes:
      continue

    rects += [x.core.GetGeometry(child).reply()]

  return rects

def obscure_image(image):
  """ Obscures the given image. """
  size = image.size
  pixel_size = 9
  if size[0] < pixel_size or size[1] < pixel_size:
    return image

  image = image.resize((size[0] // pixel_size, size[1] // pixel_size), Image.NEAREST)
  image = image.resize((size[0], size[1]), Image.NEAREST)

  print(image.mode, file=sys.stderr)
  print(image.size, file=sys.stderr)
  try:
    image = ImageEnhance.Brightness(image).enhance(BRIGHTNESS_FACTOR)
    image = ImageEnhance.Contrast(image).enhance(CONTRAST_FACTOR)
  except Exception as e:
    print(e, file=sys.stderr)

  return image

def obscure(rects):
  """ Takes an array of rects to obscure from the screenshot. """
  image = Image.open(TARGET_PATH)

  for rect in rects:
    area = (
      rect.x, rect.y,
      rect.x + rect.width,
      rect.y + rect.height
    )
    print("({}, {}), ({}, {})".format(*area), file=sys.stderr)

    cropped = image.crop(area)
    try:
      cropped = obscure_image(cropped)
    except ValueError:
      pass
    image.paste(cropped, area)

  image.save(TARGET_PATH)

def lock_screen():
  os.system('i3lock -u -i {}'.format(TARGET_PATH))

if __name__ == '__main__':
  # 1: Take a screenshot.
  screenshot()

  # 2: Get the visible windows.
  rects = xcb_fetch_windows()

  # 3: Process the screenshot.
  obscure(rects)

  # 4: Return success and image path
  print(TARGET_PATH)
