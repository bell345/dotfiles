#!/bin/bash
#
# Annotates an image with its filename in the top-left corner
# with ImageMagick.
#

filename=$(basename "$1")
extension=$(echo "$filename" | sed -E "s/^.+\.//g")
output="/tmp/annotated.${RANDOM}.${extension}"
out_size="1920x1080"
anno_font_family="Liberation Sans"
anno_font_size=14
anno_padding="+8+38"
extent="+0+0"

rm -f "$output"

convert "$1" -gravity center \
    -resize ${out_size}^ -crop ${out_size}+0+0 +repage \
    -family "$anno_font_family" -pointsize $anno_font_size \
    -gravity northwest -stroke '#000C' -strokewidth 2 \
    -annotate $anno_padding "$filename" \
    -stroke none -fill white \
    -annotate $anno_padding "$filename" \
    -gravity north -extent $extent \
    "$output"

echo "$output"
