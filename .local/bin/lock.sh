#!/bin/bash
#
# Blurs the contents of windows, then locks the screen.
# Works on multiple monitors. Uses i3lock for lockscreen mechanism.
#
set -e

tmpbg='/tmp/lock-sshot.png'
lockicon="$HOME/img/lock-icon.png"

obscure-screen.py "$tmpbg"
if [[ -f "$lockicon" ]]; then
    convert "$tmpbg" "$lockicon" -gravity center -composite -matte "$tmpbg"
fi
#convert "$tmpbg" -brightness-contrast -20x-20 "$tmpbg"
i3lock -e -p default -n -i "$tmpbg"
