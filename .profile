#!/bin/sh

if test -f /etc/profile; then
    . /etc/profile
fi

export XDG_CONFIG_HOME="$HOME/.config"
export PATH="$PATH:$HOME/.local/bin"
export EDITOR="vim"

if test "$DESKTOP_SESSION" = "bspwm"; then
    export _JAVA_AWT_WM_NONREPARENTING=1
fi

