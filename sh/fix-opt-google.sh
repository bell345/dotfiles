#!/bin/sh

OPT_DEST='/media/data/linux/opt'
GOOGLE_DEST='/usr/lib/google'

# Echoes command invocations as executed
_log() { (set -x; $@) }

_fix_folder() {
    qual=$1
    SRC_PATH="/opt/$qual"
    OPT_DEST_PATH="$OPT_DEST/$qual"
    LINK_DEST_PATH="/usr/lib/$qual"
    if [[ ! -L "$OPT_DEST_PATH" ]]; then
        echo "realigning..."
        if [[ -d "$OPT_DEST_PATH" ]]; then
            _log mv -f $OPT_DEST_PATH/* "$LINK_DEST_PATH"
        fi
        _log rm -rf "$OPT_DEST_PATH"
        _log ln -dsT "$LINK_DEST_PATH" "$OPT_DEST_PATH"
    fi
    if [[ -d "$SRC_PATH" ]]; then
        _log cp -rf $SRC_PATH/* "$OPT_DEST_PATH"
        _log rm -rf "$OPT_DEST_PATH"
    fi
}

if [[ -L /opt ]]; then
    echo "/opt has already been symlinked. Stop."
    exit 2
elif [[ "$UID" != 0 ]]; then
    echo "Root required to modify /opt/. Stop."
    exit 3
elif [[ ! -d "$OPT_DEST" ]]; then
    echo "$OPT_DEST/ needs to exist."
    exit 4
fi

_fix_folder google
_fix_folder vivaldi

#if [[ -d /opt/google/ ]]; then
#    #echo "Copying the contents of /opt/google/ to '$OPT_DEST/google/' ..."
#    if [[ ! -L "$OPT_DEST/google" ]]; then
#        if [[ -d "$OPT_DEST/google" ]]; then
#            _log cp -rf $OPT_DEST/google/* "$GOOGLE_DEST"
#        fi
#        _log ln -dsT "$GOOGLE_DEST" "$OPT_DEST/google"
#    fi
#    _log cp -rf /opt/google/* "$OPT_DEST/google/"
#    _log rm -rf /opt/google
#fi

#echo "Copying the contents of /opt/ to '$OPT_DEST/' ..."
for x in /opt/*; do
    _log cp -rf "$x" "$OPT_DEST/"
done
_log rm -rf /opt

#echo "Symlinking /opt to point to '$OPT_DEST'"
_log ln -dsT "$OPT_DEST" /opt

for file in /opt/google/chrome-*/chrome-sandbox; do
    if [[ ! -k "$file" ]]; then
        #echo "Setting permissions for '$file'..."
        _log chmod 4755 "$file"
    fi
done

for file in /opt/vivaldi*/vivaldi-sandbox; do
    if [[ ! -k "$file" ]]; then
        _log chmod 4755 "$file"
    fi
done

echo "All done!"

