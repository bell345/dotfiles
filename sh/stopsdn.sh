#!/bin/bash

clear
echo "====================================================================================================="
echo "*** WARNING: MISUSE OF THIS SCRIPT CAN RESULT IN MAJOR SLEEP DEPRIVATION THAT YOU'LL REGRET LATER ***"
echo "====================================================================================================="

sleep 5s

echo
echo "Make sure you want to stay up past 22:00 before continuing."
echo

sleep 2s

echo -n "Are you in the middle of a download or update (yes/no)? "
read choice
if [[ $choice != "no" ]]; then
    echo "Pause or stop the download, then shut down the computer."
    exit 1
fi

echo -n "Are you playing a game right now (yes/no)? "
read choice
if [[ $choice != "no" ]]; then
    echo "Drop a save or finish the match, then shut down the computer."
    exit 1
fi

echo -n "Are you developing some software of your own volition (yes/no)? "
read choice
if [[ $choice != "no" ]]; then
    echo "Save your work, even if the program doesn't compile or run, then shut down the computer."
    exit 1
fi

echo -n "Can whatever you are doing wait for the morning? Think hard about this one, especially if it's the weekend (yes/no): "
read choice
if [[ $choice != "no" ]]; then
    echo "Cut that shit out and go to bed."
    exit 1
fi

echo -n "Are you sure you want to ruin your sleep cycle and give up on forming a habit (yes/no)? "
read choice
if [[ $choice != "yes" ]]; then
    echo "Good. Shut down the computer right now."
    exit 1
fi

clear

echo "OK then. You'll probably regret this later..."

(set -x; systemctl --user stop autosdn.timer)

