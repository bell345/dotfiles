#!/usr/bin/python3

from os import path

from subprocess import check_output as getoutput
from os import system as run
# from subprocess import run, getoutput
from spotifydbus import player, get_metadata, get_playback_state
from sys import argv
import inspect

_directory = path.dirname(path.realpath(inspect.stack()[0].filename))
_filename = path.basename(path.realpath(inspect.stack()[0].filename))

def _get_int_from_script(script_name):
    cmd = "bash {}/{}.sh".format(_directory, script_name).split(" ")
    return int(getoutput(cmd))


def play():
    if get_playback_state() == "Paused":
        player.PlayPause()

def pause():
    if get_playback_state() == "Playing":
        player.PlayPause()

def play_pause():
    player.PlayPause()

def next():
    player.Next()

def previous():
    player.Previous()

def volume(vol_cmd):
    MAX_VOL = 65536

    parsed_vol_cmd = float(vol_cmd.replace("+", "").replace("%", "")) / 100

    old_vol = _get_int_from_script("spotify-volume")
    new_vol = old_vol

    if vol_cmd[0] == "+":
        new_vol = int(old_vol + MAX_VOL * parsed_vol_cmd)
    elif vol_cmd[0] == "-":
        new_vol = int(old_vol - MAX_VOL * parsed_vol_cmd)
    else:
        new_vol = int(MAX_VOL * parsed_vol_cmd)

    SPOTIFY_SINK_ID = _get_int_from_script("spotify-sink-id")

    run("pactl set-sink-input-volume {} {}".format(SPOTIFY_SINK_ID, new_vol))



if __name__ == "__main__":
    cmd_list = [
        "play",
        "pause",
        "playpause",
        "next",
        "previous",
        "volume"
    ]

    def bail_usage(msg):
        print(_filename + " <command> [options]")
        print("")
        print("Command can be any of the following: ")
        cmd_list_str = "  "
        for cmd in cmd_list:
            cmd_list_str += cmd + " "
        print(cmd_list_str)
        print()
        print(_filename + ": " + msg)
        exit(1)

    if len(argv) < 2:
        bail_usage("No command specified.")

    command = argv[1]
    if not command in cmd_list:
        bail_usage("Invalid command specified.")


    if command == "play":
        play()
    elif command == "pause":
        pause()
    elif command == "playpause":
        play_pause()
    elif command == "next":
        next()
    elif command == "previous":
        previous()
    elif command == "volume":
        if len(argv) < 3:
            bail_usage("The volume command takes an additional argument (percentage difference).")
        volume(argv[2])
