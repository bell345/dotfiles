#!/usr/bin/python3

import dbus

MUSIC_OBJECT="org.mpris.MediaPlayer2.spotify"
OBJECT_PATH="/org/mpris/MediaPlayer2"
PROPERTY_INTERFACE="org.freedesktop.DBus.Properties"
PLAYER_INTERFACE="org.mpris.MediaPlayer2.Player"

bus = dbus.SessionBus()
spotify = bus.get_object(MUSIC_OBJECT, OBJECT_PATH)
properties = dbus.Interface(spotify, PROPERTY_INTERFACE)
player = dbus.Interface(spotify, PLAYER_INTERFACE)

def get_metadata():
    md = properties.Get(PLAYER_INTERFACE, "Metadata")
    new_md = md.copy()
    for prop in md:
        if "xesam:" in prop:
            new_md[prop.replace("xesam:", "")] = md[prop]
        elif "mpris:" in prop:
            new_md[prop.replace("mpris:", "")] = md[prop]

    new_md["artists"] = ", ".join(new_md["artist"])
    new_md["length_s"] = new_md["length"] / 1e6
    
    return new_md

def get_playback_state():
    return properties.Get(PLAYER_INTERFACE, "PlaybackStatus")

