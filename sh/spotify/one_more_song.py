#!/usr/bin/python3

from time import sleep
from math import floor, ceil
from sys import argv
from shutil import get_terminal_size
from spotifydbus import player, get_metadata, get_playback_state
import spotifyctl

ACTIONS={
    "wait": "Waiting for",
    "shutdown": "Shutting down in",
    "sdn": "Shutting down in",
    "reboot": "Restarting in",
    "pause": "Pausing in",
    "stop": "Stopping in",
    "player": "infinity",
    "infinity": "infinity"
}

action = ACTIONS[argv[1] if len(argv) > 1 and argv[1] in ACTIONS else "wait"]

spotifyctl.volume("100%")

md = get_metadata()
def refresh_metadata():
    global md
    md = get_metadata()

def get_formatted_time(seconds):
    minutes = floor(seconds / 60)
    seconds = seconds % 60
    
    return "{0:02d}:{1:02d}".format(minutes, seconds)

def get_total_seconds(time=None, go_next=False):
    """ Obtains the number of seconds left in the currently playing track given
        the current run-time.
    """
    refresh_metadata()
    # Calculate track length and parse us value to minutes + seconds #
    unq_url = md["url"]
    
    total_seconds = ceil(md["length_s"])

    if time == None:
        # This is to reset to 0:00, since Spotify can't return the current #
        # position, nor seek; moving forward and back does the same thing. #
        player.Next()
        sleep(0.2)

        refresh_metadata()
        
        # It's an ad when the URL doesn't change on next, so wait until it does #
        if md["url"] == unq_url:
            print("Waiting on ads...")
            spotifyctl.volume("0%")
            # Ads pause when volume is decreased, so we unpause to get around it
            sleep(1)
            player.PlayPause()
            
            while md["url"] == unq_url:
                refresh_metadata()
                sleep(1)
            
            spotifyctl.volume("100%")
            if go_next:
                player.Next()
        
        player.Previous()
    else:
        parsed_time = time.split(":")
        total_seconds -= int(parsed_time[0])*60 + int(parsed_time[1])
    
    sleep(0.2)
    refresh_metadata()

    # Tell the user what's going on #
    if action != "infinity":
        print("{0} {1}...".format(action, get_formatted_time(total_seconds)))
    
    return total_seconds

def get_track_info(columns):
    refresh_metadata()
    
    left = md["artists"].upper() + " - " + md["title"]
    right = md["album"]
    spaces = (columns - (len(left) + len(right))) * " "
    full = left + spaces + right
    if len(full) > columns:
        full = full[:columns]
    return full
    
def get_progress_bar(progress, width):
    hashes = ceil(progress * width) * "#"
    spaces = floor((1 - progress) * width) * "-"
    
    full = hashes + spaces
    if len(full) > width:
        full = full[:width]
    return full

def wait_until_end(seconds):
    refresh_metadata()
    total_seconds = floor(md["length_s"])
    sec_elapsed = (total_seconds - seconds) + 1
    cs = get_terminal_size((80, 24)).columns
    
    print(get_track_info(cs))
    unq_url = md["url"]

    total_str = get_formatted_time(total_seconds)
    while total_seconds > sec_elapsed:
        elapsed_str = get_formatted_time(sec_elapsed)
        
        r = sec_elapsed / total_seconds
        prog_bar = get_progress_bar(r, (cs - 16))
        print(" {0} [{1}] {2} ".format(elapsed_str, prog_bar, total_str), end='\r')
        
        refresh_metadata()
        sleep(1)
        if not md["url"] == unq_url:
            if action == "infinity":
                break
            wait_until_end(get_total_seconds())
            break
        elif get_playback_state() != "Paused":
            sec_elapsed += 1
    
    print("")

if __name__ == "__main__":
    wait_until_end(get_total_seconds(argv[2] if len(argv) > 2 else None))

    if action == "infinity":
        sleep(1)
        while True:
            refresh_metadata()
            wait_until_end(get_total_seconds(go_next=True))
            sleep(1)
    elif action == "pause" or action == "stop":
        spotifyctl.pause()
