#!/usr/bin/env python3

from spotifydbus import get_metadata

if __name__ == "__main__":
    md = get_metadata()
    print(md["title"], md["artists"])

