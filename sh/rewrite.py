#!/usr/bin/env python3

import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please provide a filename that exists.", file=sys.stderr)

    try:
        with open(sys.argv[1], 'r+') as fp:
            contents = fp.read()
            fp.seek(0)
            fp.write(contents)

    except FileNotFoundError:
        print("Please provide a filename that exists.", file=sys.stderr)

    except OSError as e:
        print("OSError: {}".format(e))

    except Exception as e:
        print("Exception: {}".format(e))
