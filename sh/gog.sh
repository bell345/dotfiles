#!/bin/sh

export WINEPREFIX=~/.local/share/wineprefixes/gog
export WINEARCH=win32
export LIBGL_DEBUG=verbose
cd "$WINEPREFIX/drive_c/Program Files/GOG Galaxy"
wine GalaxyClient.exe
