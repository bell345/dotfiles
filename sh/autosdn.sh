#!/bin/bash

BEEP_CMD="beep -f 900 -l 100 -nf 950 -l 100 -nf 1000 -l 100 -nf 1050 -l 100 -nf 1100 -l 100 -nf 1150 -l 100 -nf 1200 -l 100 -nf 1250 -l 100"
BEEP_REPEAT=3

function tts() {
    notify-send -u critical "$@"
    echo "$@" | festival --tts
}

for i in {0..3}; do
    $BEEP_CMD
    sleep 0.1s
done

cd ~

#for i in {5..0}; do
#    tts "$i"
#    sleep 0.3s
#done

tts "Shutting down..."
systemctl poweroff
