#!/bin/sh
#
# Updates remote rsync repo with current state (without copying if remote file is newer)
#

export RSYNC_RSH=ssh
SOURCE="$HOME"
DESTINATION='sigptr.me:~/dotfiles/'

# Echoes command invocations as executed
_log() { (set -x; $@) }

# Navigate to script directory
cd $(dirname $0)

echo "If you have added a new file, the file-list will need to be updated."
echo "Update file list (y/n)? "
read choice
if [[ $choice == "y" ]]; then
    ${EDITOR:-nano} file-list
fi

_log rsync -rlptDuPz --files-from=file-list "$SOURCE" "$DESTINATION"

