#!/usr/bin/env python3

import struct
import sys
import os
from io import BytesIO

last_bg = None
last_fg = None

vga_to_ansi = [0, 4, 2, 6, 1, 5, 3, 7]

ibm437_visible = lambda c: bytes([c]).decode('ibm437').translate({
    0x01: "\u263A", 0x02: "\u263B", 0x03: "\u2665", 0x04: "\u2666",
    0x05: "\u2663", 0x06: "\u2660", 0x07: "\u2022", 0x08: "\u25D8",
    0x09: "\u25CB", 0x0a: "\u25D9", 0x0b: "\u2642", 0x0c: "\u2640",
    0x0d: "\u266A", 0x0e: "\u266B", 0x0f: "\u263C", 0x10: "\u25BA",
    0x11: "\u25C4", 0x12: "\u2195", 0x13: "\u203C", 0x14: "\u00B6",
    0x15: "\u00A7", 0x16: "\u25AC", 0x17: "\u21A8", 0x18: "\u2191", 
    0x19: "\u2193", 0x1a: "\u2192", 0x1b: "\u2190", 0x1c: "\u221F",
    0x1d: "\u2194", 0x1e: "\u25B2", 0x1f: "\u25BC", 0x7f: "\u2302",
})

def ansi_putc(c, bg, fg, invert=False):
    global last_bg
    global last_fg
    if last_bg != bg or last_fg != fg:
        print("\x1b[0m", end='')
        if last_bg != bg:
            print("\x1b[{}m".format(40 + vga_to_ansi[bg]), end='')
            last_bg = bg

        if last_fg != fg:
            print("\x1b[{}m".format(30 + vga_to_ansi[fg]), end='')
            last_fg = fg

    if invert:
        print("\x1b[7m", end='')
        print(chr(c), end='')
        print("\x1b[27m", end='')
    else:
        print(ibm437_visible(c), end='')

def main():
    contents = b''
    if len(sys.argv) < 2 or sys.argv[1] == "-":
        contents = sys.stdin.buffer.read()
    else:
        with open(sys.argv[1], 'rb') as fp:
            contents = fp.read()

    contents = BytesIO(contents)
    lines, columns, cursor_x, cursor_y = struct.unpack("4B", contents.read(4))

    for y in range(lines):
        for x in range(columns):
            buf = contents.read(2)
            (char,) = struct.unpack("H", buf)

            c, bg, fg = char & 0xFF, (char >> 12) & 0x07, (char >> 8) & 0x07
            invert = x == cursor_x and y == cursor_y
            ansi_putc(c, bg, fg, invert=invert)
        print("\n", end='')

if __name__ == "__main__":
    main()

