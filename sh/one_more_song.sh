#!/bin/bash

cd $(dirname $0)

act=${1:-wait}
python spotify/one_more_song.py $@ || exit 1

case "$act" in
'shutdown' | 'sdn')
    systemctl poweroff
    ;;
'reboot' | 'restart')
    systemctl reboot
    ;;
*)
    exit 0
    ;;
esac
