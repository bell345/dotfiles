#!/bin/bash

echo $#

while getopts ":a:" opt; do
	case $opt in
		a)
		  echo "-a triggered with: $OPTARG" >&2
		  ;;
		\?)
		  echo "Invalid option: -$OPTARG" >&2
		  exit 1
		  ;;
		:)
		  echo "Option -$OPTARG requires an argument." >&2
		  exit 1
		  ;;
	esac
done

shift $((OPTIND-1))
echo "Remaining arguments: $@"
