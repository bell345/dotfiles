#!/bin/bash
# Script for restarting a program.

killall $1 2>/dev/null && echo "killall: terminated $1" || echo "killall: $1 not running"

$@ &> /dev/null &
