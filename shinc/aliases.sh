#!/bin/sh


# -v for verbose: logs each file touched
# -i for interactive: asks first
alias rm='rm -vi'
# alias rm='trash -i'
alias cp='cp -vi'
alias mv='mv -vi'
# mkdir -p creates parent directories if they don't exist
# (regular mkdir is useless without it)
alias mkdir='mkdir -vp'

# shadows built-in `cd'; use `builtin cd' to get regular cd back
# automatically lists contents of new current directory
# (I mostly end up doing this anyway)
function cd() {
    builtin cd "$@" &&
    ls -a --color=auto
}

alias h='history'
alias j='jobs -l'
alias which='type -a'
alias ..='cd ..'

alias vi='vim'
# hardcore mode
#alias nano='vim'

# Pretty-print of some PATH variables:
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'

alias more='less'

alias du='du -kh'    # Makes a more readable output.
alias df='df -kTh'

#-------------------------------------------------------------
# The 'ls' family (this assumes you use a recent GNU ls).
#-------------------------------------------------------------
# Add colors for filetype and  human-readable sizes by default on 'ls':
alias ls='ls -vh --color --group-directories-first'
alias lx='ls -lXB'         #  Sort by extension.
alias lk='ls -lSr'         #  Sort by size, biggest last.
alias lt='ls -ltr'         #  Sort by date, most recent last.
alias lc='ls -ltcr'        #  Sort by/show change time,most recent last.
alias lu='ls -ltur'        #  Sort by/show access time,most recent last.

# The ubiquitous 'll': directories first, with alphanumeric sorting:
alias ll="ls -lv --group-directories-first"
alias lm='ll |more'        #  Pipe through 'more'
alias lr='ll -R'           #  Recursive ls.
alias la='ll -A'           #  Show hidden files.
alias tree='tree -Csuh'    #  Nice alternative to 'recursive ls' ...

#-------------------------------------------------------------
# Spelling typos - highly personnal and keyboard-dependent :-)
#-------------------------------------------------------------

alias xs='cd'
alias vf='cd'
alias moer='more'
alias moew='more'
alias kk='ll'
alias exot='exit'
alias exit]='exit'
alias exi='exit'
alias exut='exit'

. "$HOME/shinc/pkg.sh"

#-------------------------------------------------------------
# Other aliases
#-------------------------------------------------------------
alias sbash='sudo bash'
alias t='htop'
alias sdn='systemctl poweroff'
alias songsdn='python ~/one_more_song.py && systemctl poweroff'
alias reboot='systemctl reboot'
alias gsearch='bash ~/gsearch.sh'
alias wsearch='bash ~/wiki.sh'
alias rsprog='~/sh/rsprog.sh'
alias hsearch='cat ~/.bash_history | grep'
alias spotifyctl='~/sh/spotifyctl.sh'
alias one-more-song='cd ~ && bash ~/code/python/one-more-song/one_more_song.sh'

alias grep='grep -E --color=auto --binary-files=without-match --devices=skip'
alias grepo='grep -Eo'

alias hexdump='hexdump -C'

