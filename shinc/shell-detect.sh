#!/bin/sh

if [[ -n ${BASH_VERSION} ]]; then
    _shell_name='bash'
    _shell_version=${BASH_VERSION%.*}
elif [[ -n ${ZSH_VERSION} ]]; then
    _shell_name='zsh'
    _shell_version=${ZSH_VERSION}
else
    _shell_name='sh'
fi


