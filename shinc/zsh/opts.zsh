#!/bin/zsh

setopt auto_pushd           # Automatically push directory when using `cd'
setopt pushd_ignore_dups    # Ignore duplicates when using pushd
setopt no_match             # If directory expansion fails, create error
setopt all_export           # Export automatically
setopt correct              # Automatically correct spelling errors
setopt sun_keyboard_hack    # Omits trailing ' character
setopt c_bases              # 16#FF -> 0xFF

setopt vi
