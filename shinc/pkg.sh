#!/bin/sh

if [[ -n $(command -v pacman) ]]; then

    _pkg_man=pacman
    if [[ -n $(command -v yaourt) ]]; then
        _pkg_man=yaourt
    fi

    #-------------------------------------------------------------
    # Package manager (pacman) aliases -- for Arch Linux
    #-------------------------------------------------------------

    alias pkg-ins='$_pkg_man -Syu --aur'
    alias pkg-upd='$_pkg_man -Syu --aur && sudo $HOME/sh/fix-opt-google.sh 2>/dev/null'
    alias pkg-unins='$_pkg_man -Rs'
    alias pkg-query='$_pkg_man -Ss --aur'
    alias pkg-list='$_pkg_man -Q --aur | grep $@'

    if [[ -n $(command -v yaourt) ]]; then

        #-------------------------------------------------------------
        # Yaourt aliases -- Arch
        #-------------------------------------------------------------

        alias aur-ins='yaourt -S'
        alias aur-upd='yaourt -Syu'
        alias aur-unins='yaourt -Rs'
        alias aur-query='yaourt -Ss'

    fi

elif [[ -n $(command -v dnf) ]]; then

    #-------------------------------------------------------------
    # Package manager (dnf) aliases -- for Fedora
    #-------------------------------------------------------------

    alias pkg-ins='sudo dnf install'
    alias pkg-upd='sudo dnf upgrade --refresh'
    alias pkg-unins='sudo dnf remove'
    alias pkg-query='dnf search'

elif [[ -n $(command -v yum) ]]; then

    #-------------------------------------------------------------
    # Package manager (yum) aliases -- for Fedora
    #-------------------------------------------------------------

    alias pkg-ins='sudo yum install'
    alias pkg-upd='sudo yum upgrade --refresh'
    alias pkg-unins='sudo yum remove'
    alias pkg-query='yum search'

elif [[ -n $(command -v apt-get) ]]; then

    #-------------------------------------------------------------
    # Package manager (apt-get) aliases -- for Debian/Ubuntu
    #-------------------------------------------------------------

    alias pkg-ins='sudo apt-get install'
    alias pkg-upd='sudo apt-get dist-upgrade'
    alias pkg-unins='sudo apt-get remove'
    alias pkg-query='apt search'

fi
