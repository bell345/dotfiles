set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set number
set showcmd     " Show last cmd in statusline
set nohlsearch  " Having to type :noh every now and again is annoying
syntax on
set laststatus=2
set nobackup
set nowritebackup
set noswapfile
set nocompatible
set wrap
set viminfo='10,\"100,:20,%,n$HOME/.viminfo
set t_Co=256
set backspace=2 " <BS> over everything in insert mode
set autoindent  " Adjust to indent of previous line on <ENTER>
set showmatch   " Show matching brackets
set mouse=a     " Full mouse support (tabs, scroll, clicking)
set history=80

set sessionoptions+=resize
set sessionoptions+=winpos
set sessionoptions+=folds
set sessionoptions+=tabpages

filetype plugin on

set ruler
set go-=T
set ch=1

set t_Co=256
colorscheme elflord
set background=dark

" Powerline config
python3 from powerline.vim import setup as powerline_setup;
python3 powerline_setup()
set rtp+=/usr/lib/python3.6/site-packages/powerline/bindings/vim

if has('gui_running')
    win 200 60
    set background=dark
    colorscheme solarized
    set guifont=Iosevka\ 11

    set lines=100 columns=200
endif

" vim -b : edit binary using xxd-format!
augroup Binary
  au!
  au BufReadPre  *.bin let &bin=1
  au BufReadPost *.bin if &bin | %!xxd
  au BufReadPost *.bin set ft=xxd | endif
  au BufWritePre *.bin if &bin | %!xxd -r
  au BufWritePre *.bin endif
  au BufWritePost *.bin if &bin | %!xxd
  au BufWritePost *.bin set nomod | endif
augroup END
